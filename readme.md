# **Work in Progress**

# PySorter - Sort your Pictures the easy way

## About
This applications aims to provide an easy way to sort your pictures. It allows to filter by filetype and to define different target folgers with defined shortcuts.

Think of it like a pictureviewer that cycles through your photos and allows you to easily tell the computer in which folder this picture belongs.
This application will help you sorting through your thousands of backed up familypictures, your smartphone pictures as well as your meme dumps.

## Progress
At the moment, there's no GUI. It comes with a TUI, but due to the visual nature of the program, it isn't really usable that way.
A GUI will be included after the logic works.

## Specialties
I'm a programming beginner and this is my first bigger project. I've aimed to make the code robust and very customizable.
For example, all the menus and pre-settings get generated from easy-to-edit lists and dictionaries.
I tried to hardcode nothing, but to generate and to adopt to the changing input that comes by this.