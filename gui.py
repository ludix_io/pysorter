from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QMainWindow, QApplication
import sys

def clicked():
    print("Clicked")


def window():
    app = QApplication(sys.argv)
    win = QMainWindow()
    win.setGeometry(3000, 100, 300, 300) # xpos, ypos, width, height of Window
    win.setBaseSize(100, 100)
    win.setWindowTitle("PySorter")

    label = QtWidgets.QLabel(win)
    label.setText("my first Label")
    label.move(50, 50)

    b1 = QtWidgets.QPushButton(win)
    b1.setText("Click Me")
    b1.clicked.connect(clicked)

    win.show()
    sys.exit(app.exec_())

window()