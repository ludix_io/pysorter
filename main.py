import os

# The main menu and it's respective actions are defined in this dict.
# The createMainMenuList function generates a list from the keys.
# The mainmenu function numerates it and calls the action depending
# regarding to the users input.
# To expand the list, just add an option:action pair.
mainMenuDict = { 'Define Working Folder': 'main()',
                 'Define File Extensions': 'pictureFinder(workdir)',
                 'Define target folders': 'folderDefinitionMenu()',
                 'Sort files': "pictureWalker(workdir)"}

# this list gets populated by the createMainMenuList function.
mainMenuList = []

# for easy expandability, the possible filetypes are defined in this
# list. The get displayed as a menu by the pictureFinder function.
# To add more filetypes, just add them to the list.
# Column 1: pretty name, column 2:file extensions
fileExtension = [['JPEG', 'jpg,jpeg,jfif,spiff,jng'],
                 ['TIFF', 'tiff'],
                 ['GIF', 'gif'],
                 ['Bitmap', 'bmp'],
                 ['PNG', 'png'],
                 ['WebP', 'webp']]

# Value is returned via the pathValidator function
# workdir = ""
workdir = "/home/ivan/Pictures" #TESTING

# This list gets populated in the pictureFinder function
fileTypes = []

# This dictionary gets populated by the folderDefinitionMenu function.
# sortFolders = {}
sortFolders = {"a": "/home/ivan", "b": "/home/ivan/Pictures", "c": "/home"} #TESTING

def createMainMenuList():
    """ the main menu and it's respective options are defined in the
     mainMenuDict dictionary. As you can't iterate over the keys of
     a dict, it's necessary to generate a list from the keys. """

    for i, option in enumerate(mainMenuDict, 1):
        mainMenuList.append(option)


class fileType():
    """ constructor for fileType class. Every file type is its own
    instance, which are generated from the fileExtension list."""

    def __init__(self, prettyName, fileExtensions):
        self.prettyName = prettyName
        self.fileExtensions = fileExtensions


def main():
    """ Requires Input of a working directory if none is set already.
    Displays the main menu if already set or after defining one"""

    global workdir
    # if not workdir: #TESTING
    #    workdir = pathValidator()
    #    print("Changed workdir to ", workdir, "\n")
    mainMenu()


def mainMenu():
    """ The main menu is generated from the mainMenuDict dictionary. """

    createMainMenuList()

    for position, option in enumerate(mainMenuList, 1):
        print(position, option)
        if position == len(mainMenuDict):
            print(position + 1, "Exit PySort\n")

    while True:
        mainMenuOption = UserInput.integer("Please choose your option")
        if mainMenuOption in range(0, int(len(mainMenuDict) + 1)):
            exec(mainMenuDict[mainMenuList[mainMenuOption - 1]])
        elif mainMenuOption == int(len(mainMenuList) + 1):
            exit(2)
        else:
            print("Invalid Input. Please try again.")


def pathValidator():
    """ Checks if user defined path is valid """

    while True:
        path = UserInput.string("Please define Picture Folder")

        if not path[len(path) - 1] == "/":
            path = path + "/"

        if os.path.exists(path):
            os.chdir(path)
            return path
        else:
            print("Invalid path entered. Please try again.")


class UserInput:
    """ This class allows straight-forward input prompts.
    If an integer is expected and the user enters a string or float,
    it'll display an error message instead of terminating the script.
    Usage:  (var = ) UserInput.integer|string("Prompt Message") """

    def integer(message):
        """ Asks for and validates integer input """

        while True:
            try:
                answerInt = int(input(message + ": "))
            except (ValueError):
                print("Please enter a number.\n")
            else:
                return answerInt

    def string(message):
        """ Asks for and validates string input """

        while True:
            try:
                answerStr = str(input(message + ": "))
            except:
                print("Invalid Input.\n")
            else:
                return answerStr

    def yesno(message):
        """ Asks for and validates "Y/N" input """

        while True:
            answerYesNo = input(message + ": ")
            if answerYesNo.lower() == "y":
                return True
            elif answerYesNo.lower() == "n":
                return False
            else:
                print("Invalid Input. \n")

    def singleAlpha(message):
        """ Asks for and validates single letter input. Input of "0" returns false. """

        while True:
            answerSingleAlpha = str(input(message + ": "))

            if not len(answerSingleAlpha) == 1:
                print("Error: Entered more than a single character.\n"
                        "Please enter a singe alphabetic character. \n")
                continue
            elif answerSingleAlpha == "0":
                print("Exit...")
                return False
            elif len(answerSingleAlpha) == 1:
                try:
                    int(answerSingleAlpha)
                except:
                    return answerSingleAlpha
                else:
                    print("Error: Input is not a letter. \n"
                          "Please enter a singe alphabetic character. \n")
                    continue


def pictureFinder(path):
    """ Creating Class Instances from fileExtensions-List """
    global fileTypes

    for pName, fExt in fileExtension:
        vars()[pName] = fileType(pName, fExt)

    print("\nPlease define filetypes to include: ")
    for i in range(0, len(fileExtension), 1):
        print(i + 1, eval((f'{fileExtension[i][0]}.prettyName')))
        if i + 1 == len(fileExtension):
            print(i + 2, "Exit\n")

    while True:
        chosenFileType = UserInput.integer("Filetype to include")

        if int(chosenFileType) in range(0, len(fileExtension) + 1):
            fileTypes.append(eval((f'{fileExtension[int(chosenFileType) - 1][0]}.fileExtensions')))
            print(eval((f'{fileExtension[int(chosenFileType) - 1][0]}.prettyName')),
                  "added to list of included filetypes.")
            print(fileTypes)
        elif int(chosenFileType) == len(fileExtension) + 1:
            print("Exit Submenu \"Filetypes\"...\n\n")
            mainMenu()
        else:
            print("Invalid Input. Please try again")


def folderDefinitionMenu():
    """ This function allows to add custom shortcuts to sort the pictures into """

    print("Please define the folders you want to sort the pictures into")
    while True:

        print("Already defined shortcuts and folders:")

        try:
            for shortcut in sorted(sortFolders):
                print(shortcut.upper(), "\t" ,sortFolders[shortcut])
        except:
            print("No shortcuts and folders have been defined yet.")

        shortcut = UserInput.singleAlpha("Define the Shortcut first (single letter, A-Z)\n0 for exit.")

        if not shortcut: # Exits if input is "0" and UserInput function returns False
            break

        folder = pathValidator()

        if shortcut.lower() in sortFolders.keys() or folder in sortFolders.values():
            print("Shortcut or folder already used.")
        else:
            sortFolders[shortcut.lower()] = folder

        answer = UserInput.yesno("")
        if answer:
            continue
        else:
            break


def pictureWalker(workdir):
    """ This function cycles through the pictures that meet the users criteria """

    files = []
    files = os.listdir(workdir)

    for i in range(0, len(files)):
        print("\nFile to sort:")
        print(files[i], "\n")
        print("Which folder should the file be moved to?")

        for shortcut in sorted(sortFolders):
            print(shortcut.upper(), "\t" ,sortFolders[shortcut])

        print("0", "\t", "Exit\n")

        answer = input()

        try:
            sortFolders[answer]
        except:
            if answer == "0":
                break
            print("Invalid Input.")
        else:
            print("Moving ", files[i], " to folder ", sortFolders[answer])


def pictureMover(srcpath, trgtpath, file):
    """ This function moves the pictures based on userinput """
    os.rename(srcpath + file, trgtpath + file)


main()
